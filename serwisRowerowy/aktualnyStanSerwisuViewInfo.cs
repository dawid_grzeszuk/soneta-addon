﻿using Soneta.Business;
using Soneta.Business.UI;
using serwisRowerowy;
using Soneta.CRM;

[assembly: FolderView("Serwis rowerowy/Aktualny stan serwisu",
    Priority = 11,
    Description = "Towary ulubione osoby kontaktowej",
    TableName = "RoweryWSerwisie",
    ViewType = typeof(AktualnyStanserwisuViewInfo)
)]
namespace serwisRowerowy
{
    public class AktualnyStanserwisuViewInfo : ViewInfo
    {

        public AktualnyStanserwisuViewInfo()
        {
            ResourceName = "aktualnyStanserwisu";
            InitContext += InicjowanieKontekstu;
            CreateView += TworzenieWidoku;
        }

        void InicjowanieKontekstu(object sender, ContextEventArgs args)
        {
            args.Context.TryAdd(() => new WParams(args.Context));
        }

        void TworzenieWidoku(object sender, CreateViewEventArgs args)
        {
            WParams parameters;
            if (!args.Context.Get(out parameters))
                return;
            args.View = ViewCreate(parameters);
            args.View.AllowNew = true;
        }

        protected View ViewCreate(WParams pars)
        {
            var dx = FirmaModule.GetInstance(pars.Context.Session);
            var view = dx.RoweryWSerwisie.CreateView();
            return view;
        }
    }
    public class WParams : ContextBase
    {
        private const string Key = "Soneta.Examples.TowaryUlubione";

        public WParams(Context context) : base(context)
        {
        }
        public KontaktOsoba KontaktOsoba
        {
            get
            {
                if (Context.Contains(typeof(KontaktOsoba)))
                    return (KontaktOsoba)Context[typeof(KontaktOsoba)];
                return null;
            }
            set => Context[typeof(KontaktOsoba)] = value;
        }
    }
}