﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using serwisRowerowy;
using serwisRowerowy.Annotations;
using Soneta.Business.Licence;
using Soneta.Business.UI;
using Soneta.CRM;

[assembly: FolderView("Serwis rowerowy",
    Priority = 10,
    Description = "Przykład implementacji enova365",
    BrickColor = 153,
    Icon = "Konfiguracja",
    SingleBrick = true,
    Contexts = new object[] { LicencjeModułu.All }
)]

[assembly: FolderView("Serwis rowerowy/Statystyki",
    Priority = 11,
    Description = "Przykład implementacji enova365",
    ObjectType = typeof(ShopAddon),
    ObjectPage = "ShopAddon.Statystyki.pageform.xml",
    ReadOnlySession = false,
    ConfigSession = false,
    BrickColor = 153
)]

[assembly: FolderView("Serwis rowerowy/Cennik usług i części", Priority = 11, Description = "Aktualny cennik", TableName = "CennikUsICz")]

namespace serwisRowerowy
{
    public sealed class ShopAddon : INotifyPropertyChanged
    {
        #region Variables
        public bool Wydruk { get; set; }
        public bool WyslijMaila { get; set; }
        #endregion

        #region Methods
        public MessageBoxInformation Method() => new MessageBoxInformation("Test")
            { Text = "Test", YesHandler = () => "YES", NoHandler = () => "NO" };

        #endregion

        #region Additional Methods
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }  
}
